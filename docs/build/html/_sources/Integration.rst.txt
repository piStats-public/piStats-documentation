#########################
Analytics Integration 
#########################


***************************************
CMS Integration
***************************************

When a new article is published you can use the following API, to push it to piStats:

API Endpoint:  http://events.pi-stats.com/content

.. code-block:: json

  {
	  "articleId" : ,
	  "language" : ,
	  "format" : ,
	  "thumbnail" : ,
	  "propertyId" : ,
	  "title" : ,
	  "url" : ,
	  "publicationDate" : ,
	  "authorName" : ,
	  "authorEmail" : ,
	  "authorDesignation" : ,
	  "authorImage" : ,
	  "categoryList" : [],
	  "sectionList" : [],
	  "editorList" : [],
	  "tagList" : [],
	  "description" : 	
  }


+------------------------+------------------------+------------------------+------------------------+
| Field                  | Description            | Datatype               | Example                |
+========================+========================+========================+========================+
| articleId              | Unique id of the       | string                 | 'c059D45'              |
|                        | article                |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| language               | Article Language       | string                 | 'english', 'en'        |
|                        |                        |                        | , 'hindi'              |
+------------------------+------------------------+------------------------+------------------------+
| format                 | Article format/type    | string                 | 'video', 'gallery'     |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| thumbnail              | Article thumbnail URL  | string                 | ‘http://imageurl’      |
|                        | article                |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| propertyId             | Property id            | string                 | 'client-live'          |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| title                  | Article headline       | string                 | 'Election Results Live'|
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| url                    | Article URL            | string                 | 'http://article.com'   |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| publicationDate        | Article publication    | date                   | ‘2017-05-15T12:09:09Z’ |
|                        | date                   |                        |                        |
|                        | (UTC format)           |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| loginAuthorName        | Author name            | string                 | 'Author Name'          |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| authorEmail            | Author email           | string                 | 'author@greatnews.com' |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| authorDesignation      | Author designation     | string                 | ‘editor’               |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| authorImage            | Author image url       | string                 | 'http://authorimageur' |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| categoryList           | Article category list  | list of string         | ['India', 'Business']  |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| sectionList            | Article section list   | list of string         | ['video', 'gallery']   |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| editorList             | Article author list    | list of string         | ['author1', 'author2'] |
|                        | (multiple contributors)|                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| description            | A short description of | string                 | 'Election Results Live'|
|                        | Article                |                        |                        |
+------------------------+------------------------+------------------------+------------------------+


***************************************
Javascript
***************************************

Tracking events on web is a two step process:

  1. Include Javascript library: To start tracking the analytics events, please include the JavaScript library on all the pages which should be tracked. The library would be loaded asynchronously with the given code snippet.

      * JavaScript Library: pistats-lib-analytics.pi-stats.com/library.min.js

        * init( <property-id> , true ) : This method needs to be called on every page.

          * <property-id>: pass property-id as the first argument
          * <true/false>: pass ‘true’ to enable automatic tracking for the page. ‘false’ to track manually.

        * load(): If ‘false’ is passed in the ‘init’ method, then load() method needs to be called manually on each page after init method has been called.


  2. Expose variables on page: Expose the following variables on each page. piStats would automatically pick up these variables.

       * ‘site_lang’ : Language of the site, for example ‘hindi’, ’tamil’ etc.
       * ‘site_story_id’: Unique article id which is being read. On pages where article id might not be available, a unique id can be returned. For example, on home page article id can be ‘homepage’.


-------------------------
Note: 
-------------------------
Before calling the init and load methods, please make sure the piStats library has loaded. The best place to include the library is the 'head' section and the methods can be called anywhere after that, for example in the footer.


.. code-block:: html
   
   <script src="http://pistats-lib-analytics.pi-stats.com/library_analytics.min.js" async>
   </script>
	
.. code-block:: javascript

   piStats.init(<property-id>,true);
   piStats.load("Page Load");
   

***************************************
Android
***************************************
1. Add the following permissions:

.. code-block:: java

   <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>  
   <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>    
   <uses-permission android:name="android.permission.LOCATION"/>		    
   <uses-permission android:name="android.permission.INTERNET"/>

2. Add Firebase SDK: Please follow the instructons in the link below:
    https://firebase.google.com/docs/cloud-messaging/android/client

3. Add piStats SDK to your app:

   * In Android studio ‘File > New > New Module’ → Select ‘Import .jar/.aar’ package
   * import piStats.aar
   * In build.gradle add:
   
.. code-block:: java

  compile project(':pistats')

4. Add the following in ‘onCreate()’ of the application

.. code-block:: java

  PiStats.getInstance().init(this);




^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After piStats SDK has been initialized we can track the following type of events:



1. Registration : 

* After we have received the FCM registration token we need to register the device with piStats for push notification. 

.. code-block:: java

   public class PistatsRegistrationReceiver extends BroadcastReceiver {

   @Override
    public void onReceive(Context context, Intent intent) 
    {
        // Call Registration Event with your firebase token
        // Call Subcription Event with your selected or default language as well as selected topics 
    }
   }


* Register the broadcast receiver in manifest

.. code-block:: xml

    <receiver
      android:name=".PistatsRegistrationReceiver"
      android:enabled="true"
      android:exported="true"
      tools:ignore="ExportedReceiver">
    
       <intent-filter>
          <action android:name="com.pistats.registration.action"/>
       </intent-filter>
    </receiver> 

* Call registration event

.. code-block:: java

    Event event = new Event();
    
    event.setPropertyId(<PropertyId>);
    event.setLangauageUser(<Language>);
    event.setRegistrationId(<FCMToken>);
    
    PiStats.getInstance().registrationEvent(event);



2. Subscription: Subscription event is used to subscribe and unsubscribe users to push notification topics

.. code-block:: java

   Event event = new Event();
	
   event.setPropertyId(<PropertyId>);
	  event.setLangauageUser(<Language>);
	  event.setRegistrationId(<FCMToken>);
	  ArrayList<String> arrayListForSubscription=new ArrayList<>();

    /* Subscribe to topics */

	arrayListForSubscription.add(<Topic1>);
	arrayListForSubscription.add(<Topic2>);
	arrayListForSubscription.add(<Topic3>);
	event.setTopicArrayListForSubscribe(arrayListForSubscription);
	ArrayList<String> arrayListForUnSubscription = new ArrayList<>();
    
    /* Unsubscribe from topics */

    arrayListForUnSubscription.add(<Topic1>);
	arrayListForUnSubscription.add(<Topic2>);
	arrayListForUnSubscription.add(<Topic3>);
	arrayListForUnSubscription.add(<Topic4>);

    event.setTopicArrayListForUnsubscribe(arrayListForUnSubscription);
	
    PiStats.getInstance().subscriptionEvent(event);

3. Load: This event is used to track all the different pages/screens a user visits. This would be the most frequently used event in the app. Not only does this event keep track of user browsing history but also combines referral information as well.

.. code-block:: java

    LoadEvent event = new LoadEvent();
	
	  event.setPropertyId(<PropertyId>);
	  event.setLangauageUser(<Language>);
	  event.setContentId("contentId");
	  event.setReferrerType(<ReferrerType>);
	  event.setReferrerOrigin(<ReferrerOrigin>);
	  event.setReferrerMedium(<ReferrerMedium>);

	PiStats.getInstance().pageLoadEvent(event);



4. Opt-out: In some apps user preference includes switching off the push notification completely. If your app supports such an option, use this event to stop push notifications for the particular user.

.. code-block:: java

   Event event = new Event();
	
    event.setPropertyId(<PropertyId>);
	event.setLangauageUser(<Language>);
	event.setRegistrationId(<FCMToken>);
	ArrayList<String> arrayListForTopicUnSubscribe=new ArrayList<>();

    /*Add topics to unsubscribe*/

	arrayListForTopicUnSubscribe.add(<Topic1>);
	arrayListForTopicUnSubscribe.add(<Topic2>);
	arrayListForTopicUnSubscribe.add(<Topic3>);
	event.setTopicArrayListForUnsubscribe(arrayListForTopicUnSubscribe);

   PiStats.getInstance().optOutEvent(event);


5. Opt-in: Opposite of the "opt-out" event if the user selects the option to receive notifications in the app, call this event to switch the push notifications back on for the user.

.. code-block:: java

   Event event = new Event();

	event.setPropertyId(<PropertyId>);
	event.setLangauageUser(<Language>);
	event.setRegistrationId(<FCMToken>);
	ArrayList<String> arrayListForTopicSubscribe=new ArrayList<>();

    /*Add topics to subscribe*/

	arrayListForTopicSubscribe.add(<Topic1>);
	arrayListForTopicSubscribe.add(<Topic2>);
	arrayListForTopicSubscribe.add(<Topic3>);
	event.setTopicArrayListForSubscribe(arrayListForTopicSubscribe);

   PiStats.getInstance().optInEvent(event);



***************************************
iOS
***************************************

1. Add the following permissions in the app’s plist file:

 * Location
 * Notifications

2. Add piStats SDK to the X-code project


3. Add Firebase SDK: Please follow the instructons in the link below:
   https://firebase.google.com/docs/cloud-messaging/ios/client


4. Import piStats in AppDelegate.m

.. code-block:: c

   import <pistats/PistatsManage.h>

5. Initialize piStats in application:didFinishLaunchingWithOptions

.. code-block:: c

   (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 

   {  
   
    [PistatsManage getInstance];  
   
   }

6. Implement observer: Add kPiStatsObserverNotification in application:didFinishLaunchingWithOptions

.. code-block:: c

   (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
     {
          [PistatsManage getInstance];
          [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pistatObserverCall:)  
          name:kPiStatsObserverNotification object:nil];
     }
   (void)pistatObserverCall:(NSNotification*)notification
     {
          [self registrationCallForPistats];  
          [self subscriptionCallForPistats];  
     }

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After piStats SDK has been initialized we can track the following type of events:

1. Registration : 

* After we have received the FCM registration token we need to register the device with piStats for push notification. 

.. code-block:: c

   NewEvent *event           = [[NewEvent alloc]init];
   
        event.Language               = @“Language name”,
        event.SubscribeToTopics      = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
        event.FCMToken               = @“Firebase token” 
        event.UnsubscribeFromTopics  = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
        event.PropertyId             = PiStatsPropertyid;
        event.deviceUDID             = @“device UDID”;  
   
   [[PistatsManage getInstance]registerForDevice:PiStatsPropertyid WithEvent:event];

2. Subscription: Subscription event is used to subscribe and unsubscribe users to push notification topics

* For push notifications the subscription call is best placed as a callback from FCM registration

.. code-block:: c

   (void)tokenRefreshNotification:(NSNotification *)notification
     {
         if(![CurrentFCMPushToken isEqualToString:[userDefaults objectForKey:FCMPushToken])
          {
             [[PistatsManage getInstance]registerForDevice:PiStatsPropertyid WithEvent:event]; // Registration call for piStats 
          }
     }

* piStats subscription

.. code-block:: c
 
    NewEvent *event             = [[NewEvent alloc]init];
   
      event.Language                = @“Language name”,
      event.SubscribeToTopics         = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
      event.FCMToken              = @“Firebase token”
      event.UnsubscribeFromTopics     = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
      event.PropertyId              = PiStatsPropertyid;
      event.deviceUDID              = @“device UDID”;

   [[PistatsManage getInstance]subscribeForNotification:PiStatsPropertyid WithEvent:event];

3. Load: This event is used to track all the different pages/screens a user visits. This would be the most frequently used event in the app. Not only does this event keep track of user browsing history but also combines referral information as well.

.. code-block:: language

    NewEvent *event       = [[NewEvent alloc]init];
    
        event.Language        = @“Language name”;
        event.ReferrerOrigin  = @"internal";
        event.ReferrerMedium  = @"page";
        event.ReferrerType    = @“type”;
        event.EventTimestamp  = @“Timestamp” /* ISO date and time formate
        event.PropertyId      = PiStatsPropertyid;
        event.ContentID       = @“ContentID”;
        event.deviceUDID      =  @“device UDID”;

    [[PistatsManage getInstance]loadviewController:self WithEvent:event];

4. Opt-out: In some apps user preference includes switching off the push notification completely. If your app supports such an option, use this event to stop push notifications for the particular user.

.. code-block:: c
   
    NewEvent *event             = [[NewEvent alloc]init];
    
          event.Language              = @“Language name”,
          event.SubscribeToTopics        = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
          event.FCMToken                 = @“Firebase token”
          event.UnsubscribeFromTopics   = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
          event.PropertyId               = PiStatsPropertyid;
          event.deviceUDID               = @“device UDID”;

    [[PistatsManage getInstance]deviceSwitchOffForNotification:PiStatsPropertyid WithEvent:event];


5. Opt-in: Opposite of the "opt-out" event if the user selects the option to receive notifications in the app, call this event to switch the push notifications back on for the user.
   
.. code-block:: c

    NewEvent *event             = [[NewEvent alloc]init];
              
            event.Language              = @“Language name”,
            event.SubscribeToTopics        = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
            event.FCMToken                = @“Firebase token”
            event.UnsubscribeFromTopics   = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
            event.PropertyId            = PiStatsPropertyid;
            event.deviceUDID          = @“device UDID”;

    [[PistatsManage getInstance]deviceSwitchOnForNotification:PiStatsPropertyid WithEvent:event];



##############################
Push Notification Integration 
##############################
    








