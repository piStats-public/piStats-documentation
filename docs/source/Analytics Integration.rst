#########################
Analytics Integration 
#########################


***************************************
CMS Integration
***************************************

When a new article is published you can use the following API, to push it to piStats:

API Endpoint:  http://events.pi-stats.com/content

.. code-block:: json

  {
    "articleId" : ,
    "language" : ,
    "format" : ,
    "thumbnail" : ,
    "propertyId" : ,
    "title" : ,
    "url" : ,
    "publicationDate" : ,
    "authorName" : ,
    "authorEmail" : ,
    "authorDesignation" : ,
    "authorImage" : ,
    "categoryList" : [],
    "sectionList" : [],
    "editorList" : [],
    "tagList" : [],
    "body" : '',
    "description" :   ''
  }


+------------------------+------------------------+------------------------+------------------------+
| Field                  | Description            | Datatype               | Example                |
+========================+========================+========================+========================+
| articleId              | Unique id of the       | string                 | 'c059D45'              |
|                        | article                |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| language               | Article Language       | string                 | 'english', 'en'        |
|                        |                        |                        | , 'hindi'              |
+------------------------+------------------------+------------------------+------------------------+
| format                 | Article format/type    | string                 | 'video', 'gallery'     |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| thumbnail              | Article thumbnail URL  | string                 | ‘http://imageurl’      |
|                        | article                |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| propertyId             | Property id            | string                 | 'client-live'          |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| title                  | Article headline       | string                 | 'Election Results Live'|
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| url                    | Article URL            | string                 | 'http://article.com'   |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| publicationDate        | Article publication    | date                   | ‘2017-05-15T12:09:09Z’ |
|                        | date                   |                        |                        |
|                        | (UTC format)           |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| loginAuthorName        | Author name            | string                 | 'Author Name'          |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| authorEmail            | Author email           | string                 | 'author@greatnews.com' |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| authorDesignation      | Author designation     | string                 | ‘editor’               |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| authorImage            | Author image url       | string                 | 'http://authorimageur' |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| categoryList           | Article category list  | list of string         | ['India', 'Business']  |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| sectionList            | Article section list   | list of string         | ['video', 'gallery']   |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| editorList             | Article author list    | list of string         | ['author1', 'author2'] |
|                        | (multiple contributors)|                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| body                   | Article body           | string                 |  ‘article body’        |
|                        |                        |                        |                        |
+------------------------+------------------------+------------------------+------------------------+
| description            | A short description of | string                 | 'Election Results Live'|
|                        | Article                |                        |                        |
+------------------------+------------------------+------------------------+------------------------+


***************************************
Javascript
***************************************

Tracking events on web is a two step process:

  1. Include Javascript library: To start tracking the analytics events, please include the JavaScript library on all the pages which should be tracked. The library would be loaded asynchronously with the given code snippet.

      * JavaScript Library: https://pistats-lib-analytics.pi-stats.com/library.min.js

        * init( <property-id> , true ) : This method needs to be called on every page.

          * <property-id>: pass property-id as the first argument
          * <true/false>: pass ‘true’ to enable automatic tracking for the page. ‘false’ to track manually.

        * load(): If ‘false’ is passed in the ‘init’ method, then load() method needs to be called manually on each page after init method has been called.


  2. Expose variables on page: Expose the following variables on each page. piStats would automatically pick up these variables.

       * ‘site_lang’ : Language of the site, for example ‘hindi’, ’tamil’ etc.
       * ‘site_story_id’: Unique article id which is being read. On pages where article id might not be available, a unique id can be returned. For example, on home page article id can be ‘homepage’.


-------------------------
Note: 
-------------------------
Before calling the init and load methods, please make sure the piStats library has loaded. The best place to include the library is the 'head' section and the methods can be called anywhere after that, for example in the footer.


.. code-block:: html
   
   <script>
        function pistats(u, c) {
             var d = document, t = 'script',
                 o = d.createElement(t),
                 s = d.getElementsByTagName(t)[0];
             o.src = u;
             if (c) { o.addEventListener('load', function (e) { c(null, e); }, false); }
             s.parentNode.insertBefore(o, s);
        };
   </script>
   <script>
        pistats('https://pistats-lib-analytics.pi-stats.com/library_analytics.min.js', function() {
            piStats.init(<property-id>);
            piStats.load(<event_name>);
        });
   </script>
  
.. code-block:: javascript

   piStats.init(<property-id>);
   piStats.load("PageLoad");


***************************************
Android
***************************************
1. Add the following permissions:

.. code-block:: java

   <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>  
   <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>    
   <uses-permission android:name="android.permission.LOCATION"/>        
   <uses-permission android:name="android.permission.INTERNET"/>

2. Add piStats SDK to your app:

   * In Android studio ‘File > New > New Module’ → Select ‘Import .jar/.aar’ package
   * import piStats.aar
   * In build.gradle add:
   
.. code-block:: java

  compile project(':pistats')

3. Add the following in ‘onCreate()’ of the application

.. code-block:: java

  PiStats.getInstance().init(this, <PropertyId>);


4. Registration : 

.. code-block:: java

   public class PistatsRegistrationReceiver extends BroadcastReceiver {

   @Override
    public void onReceive(Context context, Intent intent) 
    {
      Event event = new Event();
        event.setPropertyId(<propertyId>);
        if (!TextUtils.isEmpty(<Language>))
            event.setLanguage(<Language>);
        PiStats.getInstance().registrationEvent(event);
    }
   }


* Register the broadcast receiver in manifest

.. code-block:: xml

    <receiver
      android:name=".PistatsRegistrationReceiver"
      android:enabled="true"
      android:exported="true"
      tools:ignore="ExportedReceiver">
    
       <intent-filter>
          <action android:name="com.pistats.registration.action"/>
       </intent-filter>
    </receiver> 


5. Load: This event is used to track all the different pages/screens a user visits. This would be the most frequently used event in the app. Not only does this event keep track of user browsing history but also combines referral information as well.

Note: For details about referral parameters please refer to: `Referral Analytics <http://pistats.readthedocs.io/en/latest//Key%20Concepts.html#referral-analytics>`_


.. code-block:: java

    LoadEvent event = new LoadEvent();
  
    event.setEventName("PageLoad");
    event.setPropertyId(<PropertyId>);
    event.setLangauage(<Language>);
    event.setContentId(<contentId>);
    event.setReferrerType(<ReferrerType>);
    event.setReferrerOrigin(<ReferrerOrigin>);
    event.setReferrerMedium(<ReferrerMedium>);
    event.setReferrerIndex(<ReferrerIndex>);

  PiStats.getInstance().pageLoadEvent(event);





***************************************
iOS - ObjectiveC
***************************************

1. Add the following permissions in the app’s plist file:

 * Location

2. Add piStats SDK to the X-code project


3. Add Firebase SDK: Please follow the instructons in the link below:
   https://firebase.google.com/docs/cloud-messaging/ios/client


4. Import piStats in AppDelegate.m

.. code-block:: c

   import <pistats/PistatsManage.h>

5. Initialize piStats in application:didFinishLaunchingWithOptions

.. code-block:: c

   (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 

   {  
   
    [[PistatsManage getInstance]initWithProperty:PiStatsPropertyid];
   
   }

6. Implement observer: Add kPiStatsObserverNotification in application:didFinishLaunchingWithOptions

.. code-block:: c

   (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
     {
          [PistatsManage getInstance];
          [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pistatObserverCall:)  
          name:kPiStatsObserverNotification object:nil];
     }
   (void)pistatObserverCall:(NSNotification*)notification
     {
          [self registrationCallForPistats];  
     }

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After piStats SDK has been initialized we need to implement:

1. Registration : 

* After we have received the FCM registration token we need to register the device with piStats for push notification. 

.. code-block:: c

   NewEvent *event           = [[NewEvent alloc]init];
   
        event.Language               = @“Language name”,
        event.PropertyId             = PiStatsPropertyid;
        event.deviceUDID             = @“device UDID”;  
   
   [[PistatsManage getInstance]registerForDevice:PiStatsPropertyid WithEvent:event];


2. Load: This event is used to track all the different pages/screens a user visits. This would be the most frequently used event in the app. Not only does this event keep track of user browsing history but also combines referral information as well.
   
Note: For details about referral parameters please refer to: `Referral Analytics <http://pistats.readthedocs.io/en/latest//Key%20Concepts.html#referral-analytics>`_

.. code-block:: c

    NewEvent *event       = [[NewEvent alloc]init];
    
        event.EventName       = @"PageLoad";
        event.Language        = @“Language name”;
        event.ReferrerOrigin  = @"internal";
        event.ReferrerMedium  = @"page";
        event.ReferrerType    = @“type”;
        event.EventTimestamp  = @“Timestamp” /* ISO date and time formate
        event.PropertyId      = PiStatsPropertyid;
        event.ContentID       = @“ContentID”;
        event.deviceUDID      =  @“device UDID”;

    [[PistatsManage getInstance]loadviewController:self WithEvent:event];


***************************************
iOS - Swift
***************************************

1. Add the following permissions in the app’s plist file:

 * Location
 * Notifications

2. Add piStats SDK to the X-code project and import two files in project bridging header

.. code-block:: c

  #import <pistats/PistatsManage.h>
  #import <pistats/NewEvent.h>


3. Add Firebase SDK: Please follow the instructons in the link below:
   https://firebase.google.com/docs/cloud-messaging/ios/client


4. Import piStats in AppDelegate

.. code-block:: c

   import pistats

5. Initialize piStats in application:didFinishLaunchingWithOptions

.. code-block:: c

   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
      {
        (PistatsManage.getInstance() as AnyObject).initWithProperty(pistatsPropertyId)
      }

   }

6. Implement observer: Add kPiStatsObserverNotification in application:didFinishLaunchingWithOptions

.. code-block:: c

   NotificationCenter.default.addObserver(self, selector: Selector(("pistatObserverCall:")), name: NSNotification.Name.piStatsObserver, object: nil)

  func pistatObserverCall(_ notification: Notification) {
   
      self.registrationCallForPistats()
      self.subscribeCallForPistats()
  }

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After piStats SDK has been initialized we can track the following type of events:

1. Registration : 

* After we have received the FCM registration token we need to register the device with piStats for push notification. 

.. code-block:: c

    if let fcmTokenDefaultValue = userDefault.value(forKey:pistatsFCMToken)
        {
            fcmTokenDefaultValue = deviceTokenValue as! String
        }
        if(currentFcmToken != savedFcmTokenDefaultValue)
        {
            let notifnEvent = NewEvent()
            notifnEvent.language = currentLanguage()
            notifnEvent.fcmToken = fcmToken  /* Firebase token Mandatory  */
            if let deviceTokenValue = userDefault.value(forKey:pistatsDeviceToken)
            {
                notifnEvent.deviceUDID = deviceTokenValue as! String
            }
            notifnEvent.proprtyId = pistatsPropertyId
            notifnEvent.eventTimestamp  = getTimeandDateInISO()
        }
            
      (PistatsManage.getInstance() as AnyObject).register(forDevice: notifnEvent)


2. Load: This event is used to track all the different pages/screens a user visits. This would be the most frequently used event in the app. Not only does this event keep track of user browsing history but also combines referral information as well.

.. code-block:: language

     let event = NewEvent()
        event.eventTimestamp = currentFormattedDate()
        event.language = currentLanguage()
        event.proprtyId = pistatsPropertyId
        event.referrerOrigin = RefferOrigin.HOME
        
        if appDelegate().isNotification == true
     {
            event.refererType    = "piStats”
            event.referrerOrigin = “pushNotification"
        event.referrerMedium = “newsContentID”
        }
       else{
            event.refererType    = "Internal"
            event.referrerOrigin = “ "     // Origin of event
            event.referrerMedium = “ ”    // Pass medium name between current screen  and origin screen
        }

      (PistatsManage.getInstance() as AnyObject).loadviewController(self, with:event)

