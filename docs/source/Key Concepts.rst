Key Concepts
======================================


***************************************
Property-Id
***************************************
Depending on how your app or websites are structured you might want to consider creating a single or multiple property-id’s. Rule of thumb is, assets that you want to track in one analytics dashboard should have one property id.

------------------------------------------------------------
Example 1: One language - multiple assets
------------------------------------------------------------

One property id: In this scenario the analytics for all the assets would be combined under one dashboard.
Multiple property id: If each asset has a different property id, each asset would have it’s own dashboard.

------------------------------------------------------------
Example 2: Multiple languages - multiple assets
------------------------------------------------------------

One property id: Same as example above, all the languages and assets would share the same dashboard. But language is one of the special attributes which can be used on each dashboard to filter the data.
Multiple property id: We can either create one property per asset (an asset can share multiple languages) or we can create one property id per language.  

***************************************
CMS Integration
***************************************

CMS integration is one of the unique features of piStats. By integrating with the CMS directly piStats is able to provide key insights which are enriched with CMS data. For example, a single article can be viewed through different URLs, but piStats can consolidate the analytics for that article because it uses a unique identifier for each article.

Similarly great insights can be derived into the author performance and effectiveness using the meta information from CMS.

***************************************
Referral Analytics
***************************************

An event happens when a user interacts with the website or mobile App. Each event has a referrer, for example a user might click on a post on FB and land on your site, in which case the referrer would be “Facebook.com”. But that is not the only information captured by piStats, in this example 

	* ReferralType: would be ‘Social’, since user came from a social referrer
	* ReferralOrigin: would be “facebook"
	* ReferralMedium: would be the URL of facebook, e.g. "http://m.facebook.com/".

Hierarchy of referrers can be used to track other things as well. For example how many people read an article from a specific widget, in which case the referrers can be used as:

	* ReferralType: ‘Internal’
	* ReferralOrigin: <Page Name>
	* ReferralMedium: <Name of Widget, section or menu>
	* ReferralIndex: <relative index of the item clicked by user>

This structure is pretty versatile and can be used to track a variety of user interactions. 


------------------------------------------------------------
Example 1: Tracking a menu item
------------------------------------------------------------

Let's say we want to track how efffectiveness of our menu items. We want to figure out which of menu's are used most by the users and does their relative positioning affects their usage. 

We can track how many clicks each menu item on "Home Page" gets as follows:

When the user clicks the first menu item on home page, we can send the following while calling the load event: 

	* ReferrerType: ‘Internal’
	* ReferrerOrigin: 'HomePage'        
	* ReferrerMedium: 'TopMenu'         
	* ReferrerIndex: '1'                
	  
Similarly for the 5th item on Menu we can send the following:

	* ReferrerType: ‘Internal’
	* ReferrerOrigin: 'HomePage'
	* ReferrerMedium: 'TopMenu'
	* ReferrerIndex: '5'

------------------------------------------------------------
Example 2: Tracking a widget
------------------------------------------------------------

Let's say we want to track how a specific widget, which has multiple stories displayed in a caraousel, on the 'IndiaNews' page. To track a widget like this we would send the following information:

	* ReferrerType: ‘Internal’
	* ReferrerOrigin: 'IndiaNews'
	* ReferrerMedium: 'PopulatStoriesCaraousel'
	* ReferrerIndex: '3'



***************************************
Push Notifications
***************************************


------------------------------------------------------------
Topic
------------------------------------------------------------

Represents a user segment that can be targeted to send push notifications. For example:
A topic can be as broad as, ‘HindiAndroid’, i.e. all Hindi language users on Android or it can be as specific as “All Delhi users who read the President election news on Android”
	
------------------------------------------------------------
User preferences
------------------------------------------------------------

In some apps user preferences drive the topic subscription and unsubscription. A couple of common examples:

	* User can choose categories for which he wants to get push notifications. For example a user might only be interested in getting notifications for Politics, Bollywood and Sports. Each of the categories in this case would correspond to one topic. When user sets his preferences, app can trigger a subscription to these topics for the user.
	* User can choose locations (City) for which he might want to get the push notifications. Same as in the above case, each city would represent 1 topic and app can trigger subscription for the user on one or more topics.

------------------------------------------------------------
Reach
------------------------------------------------------------

Push notificaion reach, is the total number of push notifications sent multiplied by total subscribers. For example, if the app has 100,000 active subscribers and 5 notificaions were sent out to all users, then the reach would be 500,000 (5 x 100,000). 

***************************************
Author Performance Index (API)
***************************************

Author performance index indicates how well an authors is doing, in comparison to other authors. To calculate the API for each author is a two step process, first we take the total pageviews for the author and divide it by the number of stories published by the author. Once we have the ratios for each author we normalize the ratios by dividing each ratio by total sum of ratios.

Let's look at an example, let's say in a single day we have the following author data:

+------------------------+------------------------+------------------------+
| Author                 | Stories Published      | Pageviews              |
+========================+========================+========================+
| Author1                | 5                      | 50,000                 |
|                        |                        |                        |
+------------------------+------------------------+------------------------+
| Author2                | 10                     | 35,000                 |
|                        |                        |                        |
+------------------------+------------------------+------------------------+
| Author3                | 7                      | 27,000                 |
|                        |                        |                        |
+------------------------+------------------------+------------------------+
| Author4                | 17                     | 17,000                 |
|                        |                        |                        |
+------------------------+------------------------+------------------------+
| Author5                | 1                      | 5,000                  |
|                        |                        |                        |
+------------------------+------------------------+------------------------+


Step:1 Ratio of pageviews to published stories for each author

* Author1 = 50,000/5 = 10,000
* Author2 = 35,000/10 = 3,500
* Author3 = 27,000/7 = 3857.14
* Author4 = 17,000/17 = 1,000
* Author5 = 5,000/1 = 5,000


Total (10,000 + 3,500 + 3857.14 + 1,000 + 5,000) = 23357.14

Step:2 Normalize the performance index for each author

* Author1 API (10,000 / 23,357) = 0.42
* Author2 API (3,500 / 23,357)  = 0.14
* Author3 API (3857.14 / 23,357) = 0.16
* Author4 API (1,000 / 23,357) = 0.04
* Author5 API (5,000 / 23,357) = .21
  
In this example Author1 has the best performance, followed by Author3 and so on.

