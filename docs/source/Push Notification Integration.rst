##############################
Push Notification Integration 
##############################

Push notification functionality depends upon piStats analytics, so please make sure the analytic integration is also completed.


***************************************
Javascript
***************************************

-------------------------
Pre-requisite
-------------------------

Before integrating push notifications, piStats library should be integrated with the site. Here are detailed steps `PiStats Integration <http://pistats.readthedocs.io/en/latest/Analytics%20Integration.html#javascript>`_


---------------------------------------
https:// implementation
---------------------------------------
1. Download the piStats SDKs from `here <https://s3.ap-south-1.amazonaws.com/pistats-artifacts/piStats-push-SDK.zip>`_.
2. Upload these files in your site’s top level root directory and make it publicly accessible.
3. Add the below code snippet to the head section of the site:

.. code-block:: java
  
    <script>
        var site_lang=<language_site>;
        var site_story_id=<static_story_id>;
        var domain = window.location.hostname;
        var config = <json_config_fcm>;
        var propertyId=<property_id>;
        var pistatCookieName = <cookie_name>;
        var topic=<array_subscription_topic>;
    </script>
    <script async src='/notification_min.js'></script>

+---------------------------+------------------------+------------------------+------------------------+
| Field                     | Description            | Datatype               | Example                |
+===========================+========================+========================+========================+
| language_site             | Language of the        | string                 | 'hindi'                |
|                           | site                   |                        |                        |
+---------------------------+------------------------+------------------------+------------------------+
| static_story_id           | Name of the page       | string                 | 'home'                 |
|                           |                        |                        |                        |
+---------------------------+------------------------+------------------------+------------------------+
| json_config_fcm           | FCM Json Configuration | string                 |                        |
|                           | (Provided by FCM)      |                        |                        |
+---------------------------+------------------------+------------------------+------------------------+
| property_id               | Property ID of         | string                 | ‘ABC’                  |
|                           | provided               |                        |                        |
+---------------------------+------------------------+------------------------+------------------------+
| cookie_name               | Name of the cookie     | string                 | 'pushNotification'     |
|                           | which would be set     |                        |                        |
|                           | on your site           |                        |                        |
+---------------------------+------------------------+------------------------+------------------------+
|array_subscription_topic   | Name of the topic to   | string                 | ['hindiWeb']           |
|                           | subscribe the user on. |                        |                        |
+---------------------------+------------------------+------------------------+------------------------+


***************************************
Android
***************************************
1. Add the following permissions:

.. code-block:: java

   <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>  
   <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>    
   <uses-permission android:name="android.permission.LOCATION"/>		    
   <uses-permission android:name="android.permission.INTERNET"/>

2. Add Firebase SDK: Please follow the instructons in the link below:
    https://firebase.google.com/docs/cloud-messaging/android/client

3. Add piStats SDK to your app:

   * In Android studio ‘File > New > New Module’ → Select ‘Import .jar/.aar’ package
   * import piStats.aar
   * In build.gradle add:
   
.. code-block:: java

  compile project(':pistats')

4. Add the following in ‘onCreate()’ of the application

.. code-block:: java

  PiStats.getInstance().init(this, <PropertyId>);




^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After piStats SDK has been initialized we can track the following type of events:



1. Registration : 

* After we have received the FCM registration token we need to register the device with piStats for push notification. 

.. code-block:: java

   public class PistatsRegistrationReceiver extends BroadcastReceiver {

   @Override
    public void onReceive(Context context, Intent intent) {
        if (!TextUtils.isEmpty(<language>))
            subscriptionEventForPiStat(<language>);
        Event event = new Event();
        event.setPropertyId(<PropertyId>);
        event.setLanguage(<language>);
        event.setRegistrationId(<FCM token>);
        event.setCustId(<customerId>);
        PiStats.getInstance().registrationEvent(event);
    }

    public static void subscriptionEventForPiStat(String language) {
        if (<user opted out for pushNotificaiton>)
            return;
        try {
            Event event = new Event();
            event.setPropertyId(<PropertyId>);
            event.setLanguage(language);
            event.setRegistrationId(<FCMToken>);
            ArrayList<String> arrayListForSubscription = new ArrayList<>();
            arrayListForSubscription.add(<TopicToSubcribe>);
            arrayListForSubscription.add(<TopicToSubcribe>);
            ......
            ......
            event.setTopicArrayListForSubscribe(arrayListForSubscription);
       
            PiStats.getInstance().subscriptionEvent(event);
        } catch (Exception e) {

        }
    }
}


* Register the broadcast receiver in manifest

.. code-block:: xml

    <receiver
      android:name=".PistatsRegistrationReceiver"
      android:enabled="true"
      android:exported="true"
      tools:ignore="ExportedReceiver">
    
       <intent-filter>
          <action android:name="com.pistats.registration.action"/>
       </intent-filter>
    </receiver> 

* Call registration event

.. code-block:: java

    Event event = new Event();
    
    event.setPropertyId(<PropertyId>);
    event.setLangauage(<Language>);
    event.setRegistrationId(<FCMToken>);
    event.setCustId(<customerId>);
    
    PiStats.getInstance().registrationEvent(event);



2. Subscription: Subscription event is used to subscribe and unsubscribe users to push notification topics

.. code-block:: java

   Event event = new Event();
	
   event.setPropertyId(<PropertyId>);
	  event.setLangauage(<Language>);
	  event.setRegistrationId(<FCMToken>);
	  ArrayList<String> arrayListForSubscription=new ArrayList<>();

    /* Subscribe to topics */

	arrayListForSubscription.add(<Topic1>);
	arrayListForSubscription.add(<Topic2>);
	arrayListForSubscription.add(<Topic3>);
	event.setTopicArrayListForSubscribe(arrayListForSubscription);
	ArrayList<String> arrayListForUnSubscription = new ArrayList<>();
    
    /* Unsubscribe from topics */

    arrayListForUnSubscription.add(<Topic1>);
	arrayListForUnSubscription.add(<Topic2>);
	arrayListForUnSubscription.add(<Topic3>);
	arrayListForUnSubscription.add(<Topic4>);

    event.setTopicArrayListForUnsubscribe(arrayListForUnSubscription);
	
    PiStats.getInstance().subscriptionEvent(event);

3. Load: This event is used to track all the different pages/screens a user visits. This would be the most frequently used event in the app. Not only does this event keep track of user browsing history but also combines referral information as well.

.. code-block:: java

    LoadEvent event = new LoadEvent();
	
	  event.setEventName("PageLoad");
	  event.setPropertyId(<PropertyId>);
	  event.setLangauage(<Language>);
	  event.setContentId("contentId");
	  event.setReferrerType(<ReferrerType>);
	  event.setReferrerOrigin(<ReferrerOrigin>);
	  event.setReferrerMedium(<ReferrerMedium>);

	PiStats.getInstance().pageLoadEvent(event);



4. Opt-out: In some apps user preference includes switching off the push notification completely. If your app supports such an option, use this event to stop push notifications for the particular user.

.. code-block:: java

   Event event = new Event();
	
    event.setPropertyId(<PropertyId>);
	event.setLangauage(<Language>);
	event.setRegistrationId(<FCMToken>);
	ArrayList<String> arrayListForTopicUnSubscribe=new ArrayList<>();

    /*Add topics to unsubscribe*/

	arrayListForTopicUnSubscribe.add(<Topic1>);
	arrayListForTopicUnSubscribe.add(<Topic2>);
	arrayListForTopicUnSubscribe.add(<Topic3>);
	event.setTopicArrayListForUnsubscribe(arrayListForTopicUnSubscribe);

   PiStats.getInstance().optOutEvent(event);


5. Opt-in: Opposite of the "opt-out" event if the user selects the option to receive notifications in the app, call this event to switch the push notifications back on for the user.

.. code-block:: java

   Event event = new Event();

	event.setPropertyId(<PropertyId>);
	event.setLangauage(<Language>);
	event.setRegistrationId(<FCMToken>);
	ArrayList<String> arrayListForTopicSubscribe=new ArrayList<>();

    /*Add topics to subscribe*/

	arrayListForTopicSubscribe.add(<Topic1>);
	arrayListForTopicSubscribe.add(<Topic2>);
	arrayListForTopicSubscribe.add(<Topic3>);
	event.setTopicArrayListForSubscribe(arrayListForTopicSubscribe);

   PiStats.getInstance().optInEvent(event);



***************************************
iOS - Objective-C
***************************************

1. Add the following permissions in the app’s plist file:

 * Location
 * Notifications

2. Add piStats SDK to the X-code project


3. Add Firebase SDK: Please follow the instructons in the link below:
   https://firebase.google.com/docs/cloud-messaging/ios/client


4. Import piStats in AppDelegate.m

.. code-block:: c

   import <pistats/PistatsManage.h>

5. Initialize piStats in application:didFinishLaunchingWithOptions

.. code-block:: c

   (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 

   {  
   
    [[PistatsManage getInstance]initWithProperty:PiStatsPropertyid];
   
   }

6. Implement observer: Add kPiStatsObserverNotification in application:didFinishLaunchingWithOptions

.. code-block:: c

   (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions 
     {
          [PistatsManage getInstance];
          [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pistatObserverCall:)  
          name:kPiStatsObserverNotification object:nil];
     }
   (void)pistatObserverCall:(NSNotification*)notification
     {
          [self registrationCallForPistats];  
          [self subscriptionCallForPistats];  
     }

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After piStats SDK has been initialized we can track the following type of events:

1. Registration : 

* After we have received the FCM registration token we need to register the device with piStats for push notification. 

.. code-block:: c

   NewEvent *event           = [[NewEvent alloc]init];
   
        event.Language               = @“Language name”,
        event.SubscribeToTopics      = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
        event.FCMToken               = @“Firebase token” 
        event.UnsubscribeFromTopics  = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
        event.PropertyId             = PiStatsPropertyid;
        event.deviceUDID             = @“device UDID”;  
   
   [[PistatsManage getInstance]registerForDevice:PiStatsPropertyid WithEvent:event];

2. Subscription: Subscription event is used to subscribe and unsubscribe users to push notification topics

* For push notifications the subscription call is best placed as a callback from FCM registration

.. code-block:: c

   (void)tokenRefreshNotification:(NSNotification *)notification
     {
         if(![CurrentFCMPushToken isEqualToString:[userDefaults objectForKey:FCMPushToken])
          {
             [[PistatsManage getInstance]registerForDevice:PiStatsPropertyid WithEvent:event]; // Registration call for piStats 
          }
     }

* piStats subscription

.. code-block:: c
 
    NewEvent *event             = [[NewEvent alloc]init];
   
      event.Language                = @“Language name”,
      event.SubscribeToTopics         = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
      event.FCMToken              = @“Firebase token”
      event.UnsubscribeFromTopics     = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
      event.PropertyId              = PiStatsPropertyid;
      event.deviceUDID              = @“device UDID”;

   [[PistatsManage getInstance]subscribeForNotification:PiStatsPropertyid WithEvent:event];


* piStats unsubscribe

.. code-block:: c
 
    NewEvent *event       = [[NewEvent alloc]init];
        event.Language        = [userDefaults valueForKey:ChannelType],
        event.SubscribeToTopics     = [[NSArray alloc]initWithObjects:currentChannel, nil];
        event.UnsubscribeFromTopics = [[NSArray alloc]initWithObjects:lastChannel, nil];
        event.FCMToken        = [userDefaults valueForKey:FCMPushToken],
        event.ProprtyId       = PiStatsPropertyid;
        event.deviceUDID      = [userDefaults valueForKey:UserUDID];        
          
    [[PistatsManage getInstance]subscribeForNotification:event];

3. Load: This event is used to track all the different pages/screens a user visits. This would be the most frequently used event in the app. Not only does this event keep track of user browsing history but also combines referral information as well.

.. code-block:: language

    NewEvent *event       = [[NewEvent alloc]init];
    
        event.EventName       = @"PageLoad";
        event.Language        = @“Language name”;
        event.ReferrerOrigin  = @"internal";
        event.ReferrerMedium  = @"page";
        event.ReferrerType    = @“type”;
        event.EventTimestamp  = @“Timestamp” /* ISO date and time formate
        event.PropertyId      = PiStatsPropertyid;
        event.ContentID       = @“ContentID”;
        event.deviceUDID      =  @“device UDID”;

    [[PistatsManage getInstance]loadviewController:self WithEvent:event];

4. Opt-out: In some apps user preference includes switching off the push notification completely. If your app supports such an option, use this event to stop push notifications for the particular user.

.. code-block:: c
   
    NewEvent *event             = [[NewEvent alloc]init];
    
          event.Language              = @“Language name”,
          event.SubscribeToTopics        = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
          event.FCMToken                 = @“Firebase token”
          event.UnsubscribeFromTopics   = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
          event.PropertyId               = PiStatsPropertyid;
          event.deviceUDID               = @“device UDID”;

    [[PistatsManage getInstance]deviceSwitchOffForNotification:PiStatsPropertyid WithEvent:event];


5. Opt-in: Opposite of the "opt-out" event if the user selects the option to receive notifications in the app, call this event to switch the push notifications back on for the user.
   
.. code-block:: c

    NewEvent *event             = [[NewEvent alloc]init];
              
            event.Language              = @“Language name”,
            event.SubscribeToTopics        = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
            event.FCMToken                = @“Firebase token”
            event.UnsubscribeFromTopics   = [[NSArray alloc]initWithObjects:@“topic1”,@“topic2”, nil];
            event.PropertyId            = PiStatsPropertyid;
            event.deviceUDID          = @“device UDID”;

    [[PistatsManage getInstance]deviceSwitchOnForNotification:PiStatsPropertyid WithEvent:event];



***************************************
iOS - Swift
***************************************

1. Add the following permissions in the app’s plist file:

 * Location
 * Notifications

2. Add piStats SDK to the X-code project and import two files in project bridging header

.. code-block:: c

  #import <pistats/PistatsManage.h>
  #import <pistats/NewEvent.h>


3. Add Firebase SDK: Please follow the instructons in the link below:
   https://firebase.google.com/docs/cloud-messaging/ios/client


4. Import piStats in AppDelegate

.. code-block:: c

   import pistats

5. Initialize piStats in application:didFinishLaunchingWithOptions

.. code-block:: c

   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
      {
        (PistatsManage.getInstance() as AnyObject).initWithProperty(pistatsPropertyId)
      }

   }

6. Implement observer: Add kPiStatsObserverNotification in application:didFinishLaunchingWithOptions

.. code-block:: c

   NotificationCenter.default.addObserver(self, selector: Selector(("pistatObserverCall:")), name: NSNotification.Name.piStatsObserver, object: nil)

  func pistatObserverCall(_ notification: Notification) {
   
      self.registrationCallForPistats()
      self.subscribeCallForPistats()
  }

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After piStats SDK has been initialized we can track the following type of events:

1. Registration : 

* After we have received the FCM registration token we need to register the device with piStats for push notification. 

.. code-block:: c

    if let fcmTokenDefaultValue = userDefault.value(forKey:pistatsFCMToken)
        {
            fcmTokenDefaultValue = deviceTokenValue as! String
        }

    if(currentFcmToken != savedFcmTokenDefaultValue)
        {
            let notifnEvent = NewEvent()
            notifnEvent.language = currentLanguage()
            notifnEvent.fcmToken = fcmToken  /* Firebase token Mandatory  */
            if let deviceTokenValue = userDefault.value(forKey:pistatsDeviceToken)
            {
                notifnEvent.deviceUDID = deviceTokenValue as! String
            }
            notifnEvent.proprtyId = pistatsPropertyId
            notifnEvent.eventTimestamp  = getTimeandDateInISO()
        }
            
      (PistatsManage.getInstance() as AnyObject).register(forDevice: notifnEvent)

2. Subscription: Subscription event is used to subscribe and unsubscribe users to push notification topics

* For push notifications the subscription call is best placed as a callback from FCM registration

.. code-block:: c

   func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        var fcmTokenDefaultValue = ""
        if let deviceTokenValue = userDefault.value(forKey:pistatsFCMToken)
        {
            fcmTokenDefaultValue = deviceTokenValue as! String
        }
        
        if(fcmToken != fcmTokenDefaultValue)
        {
            let notifnEvent = NewEvent()
            notifnEvent.language = currentLanguage()
            notifnEvent.fcmToken = fcmToken  /* Firebase token Mandatory  */
            if let deviceTokenValue = userDefault.value(forKey:pistatsDeviceToken)
            {
                notifnEvent.deviceUDID = deviceTokenValue as! String
            }
            notifnEvent.proprtyId = pistatsPropertyId
            notifnEvent.eventTimestamp  = getTimeandDateInISO()
            
            (PistatsManage.getInstance() as AnyObject).register(forDevice: notifnEvent)
            userDefault.set(fcmToken, forKey:pistatsFCMToken)
        }
    }

* piStats subscription

.. code-block:: c
 
      let event = NewEvent()
        event.language = currentLanguage()                          
        event.subscribeToTopics = subscribeTopics /* Pass array of all topics */           
        event.fcmToken = FCMTokenValue as! String
        event.propertyId = pistatsPropertyId
        event.eventTimestamp = getTimeandDateInISO()
      
      (PistatsManage.getInstance()as AnyObject).subscribe(forNotification:event)

* piStats unsubscribe


.. code-block:: c

        let notifnEvent = NewEvent()
          notifnEvent.language = currentLanguage()
          notifnEvent.subscribeToTopics = subscribeTopics /* Pass array of all subscribe topics */
          notifnEvent.unsubscribeFromTopics = unsubscribeTopics /* Pass array of all unsubscribe topics */
        if let FCMTokenValue = userDefault.value(forKey:PistatsFCMToken) /*Pass FCM token */
          {
              notifnEvent.fcmToken = FCMTokenValue as! String
          }
        if let deviceTokenValue = userDefault.value(forKey:pistatsDeviceToken)  /*Pass Device token */
          {
             notifnEvent.deviceUDID = deviceTokenValue as! String
          }
          notifnEvent.proprtyId  = pistatsPropertyId
        
        (PistatsManage.getInstance() as AnyObject).subscribe(forNotification:notifnEvent)


3. Load: This event is used to track all the different pages/screens a user visits. This would be the most frequently used event in the app. Not only does this event keep track of user browsing history but also combines referral information as well.

.. code-block:: language

     let event = NewEvent()
        event.eventTimestamp = currentFormattedDate()
        event.language = currentLanguage()
        event.proprtyId = pistatsPropertyId
        event.referrerOrigin = RefferOrigin
        
      if appDelegate().isNotification == true
     {
            event.refererType    = "piStats”
            event.referrerOrigin = “pushNotification"
        event.referrerMedium = “newsContentID”
        }
       else{
            event.refererType    = "Internal"
            event.referrerOrigin = “ "     // Origin of event
            event.referrerMedium = “ ”    // Pass medium name between current screen  and origin screen
        }
  
    
      (PistatsManage.getInstance() as AnyObject).loadviewController(self, with:event)


4. Opt-out: In some apps user preference includes switching off the push notification completely. If your app supports such an option, use this event to stop push notifications for the particular user.

.. code-block:: c
   
    var event = NewEvent()
        event.language = userDefaults.value(forKey:   ChannelType),
        event.subscribeToTopics = [currentChannel]
        event.fcmToken = userDefaults.value(forKey: FCMPushToken),
        event.proprtyId = PiStatsPropertyid
        event.deviceUDID = userDefaults.value(forKey: UserUDID)
        event.eventTimestamp = Utility.getTimeandDateInISO()
    PistatsManage.getInstance().deviceSwitchOff(forNotification: event)



5. Opt-in: Opposite of the "opt-out" event if the user selects the option to receive notifications in the app, call this event to switch the push notifications back on for the user.
   
.. code-block:: c

    var event = NewEvent()
        event.language = userDefaults.value(forKey: ChannelType),
        event.subscribeToTopics = [currentChannel]
        event.fcmToken = userDefaults.value(forKey: FCMPushToken),
        event.proprtyId = PiStatsPropertyid
        event.deviceUDID = userDefaults.value(forKey: UserUDID)
        event.eventTimestamp = Utility.getTimeandDateInISO()
    PistatsManage.getInstance().deviceSwitchOn(forNotification: event)  
